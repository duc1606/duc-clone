// primitive type ~  string , number , boolean
// reference type ~ aray, object

var menu = ["bánh canh", "bún bò", "hủ tíu"];
console.log('menu: ', menu);

// lấy số lượng hiện tại 
var soLuong = menu.length;
console.log('soLuong: ', soLuong);

// thêm phần tử vào mảng
menu.push("phở khô");
menu.push("bún chả hà nội");
menu.push("mì quãng");
console.log("menu sau khi push:", menu);
// duyệt mảng
for (var index =0; index < menu.length; index++) {
    var monAnHienTai = menu[index];
    // menu[index] , index là vị trí của mỗi phần tử trong các lần lập
    console.log('monAnHienTai: ', monAnHienTai);
}

// update gía trị của 1 pahanf tử ttrong aray
menu[0] = "bánh bèo";
console.log('menu sau khi update bánh bèo cho bánh canh: ', menu);

// crud 
// create read uodate Delets
var hienThiThongTin = function (monAn,index) {
    console.log('monAn-forEach: ', monAn, index);
}
menu.forEach(hienThiThongTin);

// pop
console.log("menu trc khi gạp pop", menu);
menu.pop();
console.log("menu sau khi gặp pop", menu);
//  đàu 0
// cuối 1



var thongTinInput = "phở khô";

// indexof trả về index đàu tiên nếu tìm tháy, ngược lại nếu ko tìm thấy thì trả về -1
var indexThongTin = menu.indexOf(thongTinInput);
console.log('indexThongTin: ', indexThongTin);
if (indexThongTin!==-1){
    menu[indexThongTin] = "bánh bao";
    console.log("menu sau khi update indexThongTin:", menu);
}

// map
var thucDon = menu.map(function (monAn) {
    return "món" + monAn;
});
console.log('thucDon: ', thucDon);





// slice
console.log('menu: ', menu);
// spliece
var indexBanhBao = menu.indexOf("Banh bao")
menu.splice(1, 1);
console.log('menu: ', menu);


var score = [10,10,10,,1,1,1,6,7,9,9,10,10];

var  resultScores = score.filter(function (number){
    return number ==10;
});
console.log('resultScores: ', resultScores);

